package com.momo.demo.controller;

import com.momo.demo.entity.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther: 钟棋
 * @Date: 2019/4/29 14:23
 * @Email: 2020131266@qq.com
 * @Describe:
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Value("${server.port}")
    private Integer port;


    @GetMapping("/{id}")
    public User getUser(@PathVariable Integer id) {
        System.out.println(port);
        User user = new User();
        user.setId(id);
        user.setNickname("momo");
        user.setPassword("123456789");
        return user;
    }

    @GetMapping("/manyparam")
    public String get(@RequestParam String username,@RequestParam String password) {
        System.out.println(username);
        System.out.println(password);
        return "123";
    }

    @PostMapping("/add")
    public String addUser(User user) {
        System.out.println(user.toString());
        return "添加用户";
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam MultipartFile file) {
        System.out.println(file.getOriginalFilename());
        return "上传文件";
    }


}
