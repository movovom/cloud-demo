package com.momo.demo.entity;

import lombok.Data;

/**
 * @Auther: 钟棋
 * @Date: 2019/4/29 14:22
 * @Email: 2020131266@qq.com
 * @Describe:
 */
@Data
public class User {

    private Integer id;

    private String password;

    private String nickname;

}
