package com.momo.demo.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.core.Ordered;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Auther: 钟棋
 * @Date: 2019/5/5 18:12
 * @Email: 2020131266@qq.com
 * @Describe:
 */
public class MyGatewayFilter implements GatewayFilter,Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        System.out.println("start-------------------------");

        exchange.getAttributes().put("start",System.currentTimeMillis());
     return   chain.filter(exchange).then(Mono.fromRunnable(() ->{
            Long startTime = exchange.getAttribute("start");
            if(startTime != null) {
                System.out.println(exchange.getRequest().getURI()+"--"+(System.currentTimeMillis()-startTime)+"ms");
            }
        }));
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
