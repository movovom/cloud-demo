package com.momo.demo;


import com.momo.demo.filter.MyGatewayFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class GateWayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GateWayApplication.class, args);
	}


	@Bean
	public RouteLocator customerRouteLocator(RouteLocatorBuilder builder) {
		return builder.routes().route(r -> r.path("/my/**").filters(f -> f.filter(new MyGatewayFilter()).addRequestHeader("MyHeader","MyHeadValue")).uri("http://localhost:7201/user/1").order(0).id("my_filter_router")).build();
	}
}
