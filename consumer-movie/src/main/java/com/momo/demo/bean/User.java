package com.momo.demo.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @Auther: 钟棋
 * @Date: 2019/4/29 14:22
 * @Email: 2020131266@qq.com
 * @Describe:
 */
@Data
public class User implements Serializable {

    private Integer id;

    private String password;

    private String nickname;

    public User(Integer id, String password, String nickname) {
        this.id = id;
        this.password = password;
        this.nickname = nickname;
    }

    public User() {
    }
}
