package com.momo.demo.feign.controller;


import com.momo.demo.feign.UserFeignClient;
import com.momo.demo.bean.User;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: 钟棋
 * @Date: 2019/4/29 14:26
 * @Email: 2020131266@qq.com
 * @Describe:
 */
@RestController
@RequestMapping("/movie")
public class MovieController1 {

    @Resource
    private UserFeignClient userFeignClient;



    @GetMapping("/feign/user/{id}")
    public User findById(@PathVariable Integer id){
        return this.userFeignClient.findById(id);
    }

    @PostMapping("/feign/add/user")
    public String  addUser(User user){
        return this.userFeignClient.addUser(user);
    }

    @GetMapping("/feign/manyparam")
    public String  manypara(@RequestParam String username,@RequestParam String password){
        return this.userFeignClient.manyPara(username,password);
    }

    @GetMapping("/feign/mapparam")
    public String  mapparam(@RequestParam String username,@RequestParam String password){
        Map<String,Object> map  = new HashMap<>();
        map.put("username","adsfsdf");
        map.put("password","fsdfsdf");
        return this.userFeignClient.mapParam(map);
    }



    @PostMapping("/feign/upload/file")
    public String  uploadFile(@RequestParam MultipartFile file){

        return null;
     //   return this.userFeignClient.handleFileUpload(file);
    }


}
