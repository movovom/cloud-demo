package com.momo.demo.feign;

import com.momo.demo.bean.User;
import feign.Logger;

import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.util.Map;

/**
 * @Auther: 钟棋
 * @Date: 2019/4/29 14:58
 * @Email: 2020131266@qq.com
 * @Describe:*/


@FeignClient(name = "provider-user", configuration = UserFeignConfig.class, fallbackFactory = UserFeignClientFallbackFactory.class)
public interface UserFeignClient {

    @GetMapping("/user/{id}")
    User findById(@PathVariable("id") Integer id);

    @PostMapping("/user/add")
    String addUser(@RequestBody User user);

    @GetMapping("/user/manyparam")
    String manyPara(@RequestParam("username") String usernamem,@RequestParam("password") String password);

    @GetMapping("/user/manyparam")
    String mapParam(@RequestParam Map<String, Object> map);

    @PostMapping(value = "/user/upload",
            produces = {MediaType.APPLICATION_JSON_UTF8_VALUE},
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    String handleFileUpload(@RequestPart(value = "file") MultipartFile file);

}

class UserFeignConfig {
    @Bean
    public Logger.Level logger() {
        return Logger.Level.FULL;
    }


    @Bean
    public Encoder feignFormEncoder() {
        return new SpringFormEncoder();
    }
}

@Component
@Slf4j
class UserFeignClientFallbackFactory implements FallbackFactory<UserFeignClient> {
    @Override
    public UserFeignClient create(Throwable throwable) {
        return new UserFeignClient() {


            @Override
            public User findById(Integer id) {
                log.error("进入回退逻辑", throwable);
                return new User(4,"回滚密码","不存在的用户")  ;
            }

            @Override
            public String addUser(User user) {
                return null;
            }

            @Override
            public String manyPara(String usernamem, String password) {
                return null;
            }

            @Override
            public String mapParam(Map<String, Object> map) {
                return null;
            }

            @Override
            public String handleFileUpload(MultipartFile file) {
                return null;
            }
        };
    }
}
