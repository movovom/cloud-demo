package com.momo.demo.ribbon.controller;


import com.momo.demo.bean.User;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @Auther: 钟棋
 * @Date: 2019/4/29 14:26
 * @Email: 2020131266@qq.com
 * @Describe:
 */
@RestController
@RequestMapping("/movie")
public class MovieController {

    @Autowired
    private RestTemplate restTemplate;


    @HystrixCommand(fallbackMethod = "findByIdFallback")
    @GetMapping("/ribbon/user/{id}")
    public User findByUserId(@PathVariable Integer id) {
        User user = restTemplate.getForObject("http://provider-user/user/{id}", User.class, id);
       return user;
    }

    public User findByIdFallback(Integer id) {
        return new User(0,"默认用户密码","默认昵称");
    }
}
