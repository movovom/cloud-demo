package com.momo.demo.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: 钟棋
 * @Date: 2019/5/5 14:39
 * @Email: 2020131266@qq.com
 * @Describe:
 */
public class MyZuulFilter extends ZuulFilter{


    private static final Logger log = LoggerFactory.getLogger(MyZuulFilter.class);


    @Override
    public String filterType() {
         /*Zuul大部分功能都是通过过滤器来实现的。Zuul中定义了四种标准过滤器类型，这些过滤器类型对应于请求的典型生命周期。
         pre：这种过滤器在请求被路由之前调用。我们可利用这种过滤器实现身份验证、在集群中选择请求的微服务、记录调试信息等。
         routing:  这种过滤器将请求路由到微服务。这种过滤器用于构建发送给微服务的请求，并使用Apache HttpClient或Netfilx Ribbon请求微服务。
         post: 这种过滤器在路由到微服务以后执行。这种过滤器可用来为响应添加标准的HTTP Header、收集统计信息和指标、将响应从微服务发送给客户端等。
         error: 在其他阶段发生错误时执行该过滤器。
         */
        return "pre";
    }

    @Override
    public int filterOrder() {
        //返回一个int值来指定过滤器的执行顺序，不同的过滤器允许返回相同的数字。
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        //返回一个boolean值来判断该过滤器是否要执行，true表示执行，false表示不执行。
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        log.info(request.getMethod()+"---"+request.getServletPath());
        //过滤器的具体逻辑。本例中，我们让它打印了请求的HTTP方法以及请求的地址。
        return null;
    }
}
